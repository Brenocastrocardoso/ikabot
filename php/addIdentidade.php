<?php

require_once "class/pessoa.class.php";
require_once "class/global.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
	if (!empty($_POST["senha"]) && !empty($_POST["cpf"]))
	{
		session_start();
		$ident_cur = unserialize($_SESSION["Identidade"]);

		if ($ident_cur->getStatus()=="Administrador"){

			$ident = new Identidade();
			if (!empty($_POST["cpf"])){
				if(!$ident->setCpf($_POST["cpf"])){
					echo '{"msg": "Não foi possivel inserir o campo CPF"", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["senha"])){
				if(!$ident->setSenha($_POST["senha"])){
					echo '{"msg": "Não foi possivel inserir o campo Senha", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["email"])){
				if(!$ident->setEmail($_POST["email"])){
					echo '{"msg": "Não foi possivel inserir o campo Email", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["nome"])){
				if(!$ident->setNome($_POST["nome"])){
					echo '{"msg": "Não foi possivel inserir o campo Nome", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["status"])){
				if(!$ident->setStatus($_POST["status"])){
					echo '{"msg": "Não foi possivel inserir o campo Status", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["nacionalidade"])){
				if(!$ident->setNacionalidade($_POST["nacionalidade"])){
					echo '{"msg": "Não foi possivel inserir o campo Nacionalidade", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["estado_civil"])){
				if(!$ident->setEstadoCivil($_POST["estado_civil"])){
					echo '{"msg": "Não foi possivel inserir o campo Estado Civil", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["rg"])){
				if(!$ident->setRg($_POST["rg"])){
					echo '{"msg": "Não foi possivel inserir o campo RG", "status:"Erro"}';
					return;
				}
			}
			if(!$ident->inserirDb())
				echo '{"msg": "Não foi possivel inserir os dados no Banco de Dados", "status:"Erro"}';
			else
				echo '{"msg": "Cadastro realizado com sucesso", "status": "Sucesso"}';
		}
		else
		{
			echo '{"msg": "Permissão Negada", "status":"Erro"}';
		}
	}
	else
	{
		echo '{"msg": "Os campos Senha e CPF são obrigatórios", "status":"Sucesso"}';
	}
}