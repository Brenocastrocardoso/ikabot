<?php

require_once "defines.php";
require_once "db.class.php";
require_once "engine_class.php";

class Usuario extends Comum implements JsonSerializable
{
	const PRIVILEGIO = array('ADMIN', 'MEMBER');

	private $login;
	private $senha;
	private $idusuarios;
	private $privilegio;
    private $players;

    public function login():bool
    {
        $db = new Db();
        
        $raw_output = $db->search("usuario", "*", array("senha" => $this->senha, "login" => $this->login));

        if (!($usuario = $raw_output->fetch()))
            return FALSE;

        $this->idusuarios=$usuario["idusuarios"];
        $this->privilegio=$usuario["privilegio"];

        session_start();

        $_SESSION["Usuario"] = serialize($this);

        return TRUE;
    }

    public function CarregarPlayers()
    {
        $db = new Db();
        
        $raw_output = $db->search("Player", "*", array("usuarios_idusuarios" => $this->idusuarios));

        $output = $raw_output->fetchAll();
        foreach ($output as $i)
        {

            $player = new Player($this->idusuarios,
                               $i["email"],
                               $i["senha"],
                               $i["nick"],
                               $i["cookies"],
                               $i["playerId"]);
            $this->addPlayer($player);
        }

        return $this->players;
    }

    public function addPlayer($nplayer)
    {
        if ($nplayer instanceof Player)
        {
            if ($this->players == NULL)
                $this->players = array();
            array_push($this->players, $nplayer);
            return TRUE;
        }
        return FALSE;
    }

    public function setLogin($nlogin)
    {
    	$nlogin = clearInput($nlogin);
        if (!empty($nlogin))
        {    
            if (preg_match('/^[a-zA-Z0-9 ]*$/',$nlogin))
            {
                $this->login = $nlogin;
                return TRUE;
            }
        }
        return FALSE;
    }
    public function setSenha($nsenha)
    {
    	$nsenha = clearInput($nsenha);
        if (!empty($nsenha))
        {            
            if (preg_match('/^[a-zA-Z0-9_@!#%&*()-+.,<> ]{6}[a-zA-Z0-9_@!#%&*()-+.,<> ]*$/',$nsenha))
            {
                $this->senha = $nsenha;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function privilegio($nPrivilegio)
    {
        foreach (Usuario::PRIVILEGIO as $i) {
            if ($nPrivilegio==$i)
            {
                $this->privilegio = $nPrivilegio;
                return TRUE;
            }
        }
        return FALSE; 
    }

    public function __get($name)
    {
    	return $this->$name;
    }

    public function notNullGeneratorDb()
    {
        yield $this->login;
        yield $this->senha;
        yield $this->privilegio;
        yield $this->idusuario;
    }

    public function notNullGenerator()
    {
        yield $this->login;
        yield $this->senha;
    }

    public function jsonSerialize()
    {
        $vars = array(
          "login"=>$this->login,
          "senha"=>$this->senha,
          "privilegio"=>$this->privilegio,
          "idusuario"=>$this->idusuario
          );

        return $vars;
    }

}

?>