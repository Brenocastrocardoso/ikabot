<?php
if(!isset($_SESSION))
    session_start();

function clearInput($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

abstract class Comum
{
    public function isValid():bool
    {
        foreach ($this->notNullGenerator() as $i)
        {
            if ($i == NULL)
                return FALSE;
        }
        return TRUE;
    }

    public function isShowable()
    {
        foreach ($this->notNullShowable() as $i)
        {
            if ($i == NULL)
                return FALSE;
        }
        return TRUE;
    }

    abstract public function notNullGenerator();
}

function validarString($String)
{
        if (!empty($String))
        {    
            if (preg_match('/^[a-zA-Z0-9-ãõâôáàéíóú.ç() ]*$/',$String))
            {
                return TRUE;
            }
        }
        return FALSE;
}

function validarData($nData)
{
    if (!empty($nData))
    {
        if (preg_match('/^20[0-9]{2}-[0-9]{2}-[0-9]{2}/',$nData))
        {
            return TRUE;
        }
    }
    return FALSE;   
}

function verificaLogin()
{
    require_once("../php/class/pessoa.class.php");

    if(isset($_SESSION['Identidade']))
    {
        $ident = unserialize($_SESSION['Identidade']);

        if(!$ident->isShowable())
            header("location: ../login.html");
        else
            return $ident;
    }else
        header("location: ../login.html");
}

?>