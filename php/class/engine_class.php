<?php

require_once "defines.php";

class Player
{

	private $cookies;
	private $actionRequest;
	private $cityId;
	private $islandId;
	private $usuarioId;
	public $nick;
	public $playerId;
	public $email;
	private $senha;


	// private $host;

	public function Player($usuarioId=null, $email=null, $senha=null, $nick=null, $cookies=null, $playerId=null)
	{
		$this->usuarioId = $usuarioId;
		$this->email = $email;
		$this->senha = $senha;
		$this->nick = $nick;
		$this->cookies = $cookies;
		$this->playerId = $playerId;
	}

	public function criar_conta(/*$email, $senha*/):bool
	{

		$this->nick = $this->gerarNome();
		$this->email = $this->nick."@gmail.com";
		$this->senha = substr(md5(microtime()),rand(0,26),8);

		$url = 'https://s40-br.ikariam.gameforge.com/index.php?action=newPlayer&function=createAvatar';

		$data = "agb=on&email=$this->email&gclid=&kid=&password=$this->senha&uni_url=s40-br.ikariam.gameforge.com";

		$datalength = strlen($data);

		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength"); 

		$page = $this->curl($url,0,$data,$header);

		$this->cookies = $this->GetCookies($page);

		// echo $page;

		if(preg_match('/actionRequest: \"(.*)\",/U', $page, $matches))
		{			
			$db = new Db();
			if($return){
			    if ($ret = $db->tryInsert("Player", 
			    							[
			                                "email" => $this->email,
			                                "senha" => $this->senha,
			                                "cookies" => $this->cookies,
			                                "usuarios_idusuarios" => $this->usuarioId,
			                                "nick" => $this->nick
			                                ], 
			                                [
			                                "email" => $this->email,
			                                "nick" => $this->nick
			                            	]))
			    {
			    	// var_dump($player);
			    }
			}
			$this->actionRequest = $matches[1];
			$sucesso = true;
		}else
		{
			$sucesso = false;
		}

		return $sucesso;

	}

	public function __get($name)
	{
		return $this->$name;
	}

	public function gerarNome()
	{
		$host = "https://rogertakemiya.com.br/gerador-de-nomes-para-jogos/";
		$seed = time();
		$data = "?gen=1&esp=0&sex=0&_=$seed";
		$url = $host . $data;

		$response = $this->curl($url,0,0);

		$pos = strrpos($response, '>');
		$nome = substr($response, $pos+2);
		$nome = strtolower($nome);
		return $nome;
	}

	public function fazerTutorial()
	{
		$return = $this->login($this->email, $this->senha);
		$return = $this->aceitarTutorial();
		$return = $this->colocarTrabalhadores(10);
		$return = $this->recolherRecompensa();
		$return = $this->construir(ACADEMIA);
		$return = $this->recolherRecompensa();


		$return = $this->pesquisarConservacao();
		$return = $this->recolherRecompensa();
		$return = $this->colocarCientistas(8);
		$return = $this->recolherRecompensa();
		$return = $this->construir(ARMAZEM);
		$return = $this->recolherRecompensa();
		$return = $this->construir(QUARTEL);
		$return = $this->recolherRecompensa();
		$uni = new Unidades(3);
		$return = $this->recrutarUnidades($uni->getPackage(), QUARTEL["position"]);
		$return = $this->construir(MURALHA);
		sleep(180);
		$return = $this->recolherRecompensa();
		$return = $this->construir(PORTO);
		$return = $this->recolherRecompensa();
		$return = $this->comprarBarcoMercante(PORTO["position"]);
		$return = $this->recolherRecompensa();
		$return = $this->melhorarConstrucao(ARMAZEM, 2, 0);
		$return = $this->recolherRecompensa();
		$return = $this->atacarBarbaros();
		$return = $this->recolherRecompensa();

		$return = $this->logout();
		$return = $this->login($this->email, $this->senha);
		$return = $this->recolherRecompensa();
		$return = $this->recolherRecompensa();
		$return = $this->recolherRecompensa();

		$return = $this->aceitarTutorial();
		$return = $this->verLoja();
		$return = $this->recolherRecompensa();
	}

	public function autoLogin()
	{
		$this->login($this->email, $this->senha, 0);
	}

	public function login($email, $senha, $tentativa=0)
	{
		if($tentativa > 5)
			return;

		$url = 'https://s40-br.ikariam.gameforge.com/index.php?action=loginAvatar&function=login';

		$data = "detectedDevice=1&kid=&name=$email&password=$senha&pwat_checksum=&pwat_uid=&startPageShown=1&uni_url=s40-br.ikariam.gameforge.com";

		// echo ($data);

		$datalength = strlen($data);

		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength"); 

		$page = $this->curl($url,0,$data,$header);

		$this->cookies = $this->GetCookies($page);

		if(preg_match('/actionRequest: \"(.*)\",/U', $page, $matches))
		{
			$this->actionRequest = $matches[1];
		}else
		{
			$this->login($email, $senha, $tentativa +1);
			return;
		}


		preg_match('/currentCityId: ([0-9]*),/U', $page, $matches);
		$this->cityId = $matches[1];

		preg_match('/\"islandId\":\"([0-9]*)\"/U', $page, $matches);
		$this->islandId = $matches[1];

	}

	public function aceitarTutorial()
	{

		$host = 'https://s40-br.ikariam.gameforge.com/index.php';

		$url = $host;

		$actionRequest = $this->actionRequest;
		$cityId = $this->cityId;
		$islandId = $this->islandId;

		$data = "action=TutorialOperations&actionRequest=".$actionRequest."&ajax=1&avatarName=$this->nick&backgroundView=city&cityId=".$cityId."&currentcityId=".$cityId."&detectedDevice=1&disableTutorial=false&function=updateTutorialData&startPageShown=1";

		// echo $data;
		// echo "<br><br>";

		$datalength = strlen($data);
		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

		$page = $this->curl($url, $this->cookies, $data, $header);

		// echo($page);

		preg_match('/\"actionRequest\":\"(.*)\",/U', $page, $matches);
		$this->actionRequest = $matches[1];
	}

	public function avancarConstrucao($location)
	{

		$host = 'https://s40-br.ikariam.gameforge.com/index.php';

		$url = $host;

		$actionRequest = $this->actionRequest;
		$cityId = $this->cityId;
		$islandId = $this->islandId;

		$data = "action=TutorialOperations&actionRequest=".$actionRequest."&ajax=1&avatarName=&backgroundView=city&cityId=".$cityId."&currentcityId=".$cityId."&detectedDevice=1&disableTutorial=false&function=updateTutorialData&startPageShown=1";

		// echo $data;
		// echo "<br><br>";

		$datalength = strlen($data);
		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

		$page = $this->curl($url, $this->cookies, $data, $header);

		// echo($page);

		preg_match('/\"actionRequest\":\"(.*)\",/U', $page, $matches);
		$this->actionRequest = $matches[1];

	}

	public function atacarBarbaros()
	{

		$data = "action=transportOperations&".
		"actionRequest=".$this->actionRequest."&".
		"ajax=1&".
		"backgroundView=island&".
		"barbarianVillage=1&".
		"cargo_army_315=6&".
		"cargo_army_315_upkeep=1&".
		"currentIslandId=".$this->islandId."&".
		"function=attackBarbarianVillage&".
		"islandId=".$this->islandId."&".
		"templateView=plunder".
		"transporter=2&";

		$this->doThings($data);	

	}

	public function verLoja()
	{

		$data = 
		"actionRequest=".$this->actionRequest."&".
		"ajax=1&".
		"backgroundView=city&".
		"currentcityId=".$this->cityId."&".
		"linkType=2&".
		"view=premium&";

		$this->doThings($data);	

	}

	public function doThings($data)
	{
		$host = 'https://s40-br.ikariam.gameforge.com/index.php';


		$url = $host;

		$actionRequest = $this->actionRequest;
		$cityId = $this->cityId;
		$islandId = $this->islandId;

		$datalength = strlen($data);
		
		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

		$page = $this->curl($url, $this->cookies, $data, $header);

		// echo $page;

		if (preg_match('/\"actionRequest\":\"(.*)\",/U', $page, $matches))
		{
			$this->actionRequest = $matches[1];
		}

		return $page;

	}

	public function doThingsKeepActionRequest($data)
	{
		$host = 'https://s40-br.ikariam.gameforge.com/index.php';


		$url = $host;

		$actionRequest = $this->actionRequest;
		$cityId = $this->cityId;
		$islandId = $this->islandId;

		$datalength = strlen($data);
		
		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

		$page = $this->curl($url, $this->cookies, $data, $header);

		// echo $page;

		// preg_match('/\"actionRequest\":\"(.*)\",/U', $page, $matches);
		// $this->actionRequest = $matches[1];

		return $page;

	}

	public function aceitarRecompensas()
	{

		$data = "action=TutorialOperations&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"detectedDevice=1&".
				"function=rewardShown&".
				"startPageShown=1";

		$this->doThings($data);		

		$data = "action=TutorialOperations&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"currentcityId=".$this->cityId."&".
				"function=rewardShown&";

		$this->doThings($data);	

		$data = "action=TutorialOperations&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"currentcityId=".$this->cityId."&".
				"function=rewardShown&";

		$this->doThings($data);		

	}

	public function recolherRecompensa()
	{
		$data = "action=TutorialOperations&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"currentcityId=".$this->cityId."&".
				"function=rewardShown&";

		$this->doThings($data);		
	}

	public function colocarCientistas($cientistas)
	{
		$data = "action=IslandScreen&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"function=workerPlan&".
				"position=9/&".
				"s=".$cientistas."&".
				"screen=academy&".
				"templateView=academy";

		$this->doThings($data);	
	}

	public function logout()
	{
		$host = 'https://s40-br.ikariam.gameforge.com/index.php';


		$data = "action=logoutAvatar&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"function=logout&";

		$url = $host ."?". $data;

		$datalength = strlen($data);
		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

		$this->curl($url, $this->cookies, 0, $header);	
	}



	// public function updateTutorialData()
	// {

	// 	$host = 'https://s40-br.ikariam.gameforge.com/index.php';

	// 	$url = $host;

	// 	$actionRequest = $this->actionRequest;
	// 	$cityId = $this->cityId;
	// 	$islandId = $this->islandId;

	// 	$data = "action=TutorialOperations&actionRequest=".$actionRequest."&ajax=1&backgroundView=city&currentcityId=".$cityId."&disableTutorial=false&function=updateTutorialData";

		// echo $data;
		// echo "<br><br>";

	// 	$datalength = strlen($data);
	// 	$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

	// 	$page = $this->curl($url, $this->cookies, $data, $header);

		// echo($page);

	// }


	public function construirAcademia()
	{

		$host = 'https://s40-br.ikariam.gameforge.com/index.php';

		$url = $host;

		$actionRequest = $this->actionRequest;
		$cityId = $this->cityId;
		$islandId = $this->islandId;

		$data = "action=CityScreen&actionRequest=".$actionRequest."&ajax=1&backgroundView=city&building=".ACADEMIA_NUMBER."&cityId=".$cityId."currentcityId=".$cityId."&disableTutorial=false&function=build&position=".ACADEMIA_POSITION."&templateView=buildingGround";

		// echo $data;
		// echo "<br><br>";

		$datalength = strlen($data);
		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

		$page = $this->curl($url, $this->cookies, $data, $header);

		// echo($page);

		preg_match('/\"actionRequest\":\"(.*)\",/U', $page, $matches);
		$this->actionRequest = $matches[1];

	}


	public function construirArmazem()
	{

		$data = "action=CityScreen&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"building=7&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"disableTutorial=false&".
				"function=build&".
				"position=5&".
				"templateView=buildingGround";

		$this->doThings($data);

		// echo $page;

	}

	public function construir($building)
	{
		$this->construirEspecial(
			$building["number"], 
			$building["position"],
			$building["speedup"]
		);
	}

	public function melhorarConstrucao($building, $level, $speedup=0)
	{
		$this->melhorarConstrucaoEspecial(
			$building["number"], 
			$building["position"],
			$level,
			$speedup
		);
	}

	public function construirEspecial($building, $position, $speedup = 0)
	{
		$data = "action=CityScreen&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"building=".$building."&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"function=build&".
				"position=".$position."&".
				"templateView=buildingGround";

		$this->doThings($data);

		for($i=0;$i<$speedup;++$i)
		{
			$this->terminarConstrucao($position);
		}
	}

	public function melhorarConstrucaoEspecial($building, $position, $level, $speedup = 0)
	{
		$data = "action=CityScreen&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"function=upgradeBuilding&". 
				"level=".$level."&".
				"position=".$position;//."&".
				//"templateView=buildingGround";

		$this->doThings($data);

		for($i=0;$i<$speedup;++$i)
		{
			$this->terminarConstrucao($position);
		}
	}

	public function comprarBarcoMercante($position)
	{
		$data = "action=CityScreen&".
				"actionRequest=".$this->actionRequest."&".
				"activeTab=tabBuyTransporter&".
				"ajax=1&".
				"backgroundView=city&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"function=increaseTransporter&".
				"position=".$position."&".	//tentar retirar depois
				"templateView=port";

		$this->doThings($data);

	}

	public function recrutarUnidades($pack_string, $position)
	{
		$data = $pack_string.
				"&action=CityScreen&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"function=buildUnits&".
				"position=".$position."&".
				"templateView=barracks";

		$this->doThings($data);
	}


	public function terminarConstrucao($position)
	{

		$data = "action=Premium&".
				"actionRequest=".$this->actionRequest."&".
				"ajax=1&".
				"backgroundView=city&".
				"cityId=".$this->cityId."&".
				"currentcityId=".$this->cityId."&".
				"function=buildingSpeedup&".
				"level=0&".
				"position=".$position;

		$page = $this->doThings($data);

		// echo $page;

	}

	public function pesquisarConservacao()
	{

		$host = 'https://s40-br.ikariam.gameforge.com/index.php';

		$url = $host;

		$actionRequest = $this->actionRequest;
		$cityId = $this->cityId;
		$islandId = $this->islandId;

		$data = "action=Advisor&actionRequest=".$actionRequest."&ajax=1&backgroundView=city&currentcityId=".$cityId."&function=doResearch&templateView=researchAdvisor&type=economy";

		echo $data;
		echo "<br><br>";

		$datalength = strlen($data);
		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

		$page = $this->curl($url, $this->cookies, $data, $header);

		echo($page);

		preg_match('/\"actionRequest\":\"(.*)\",/U', $page, $matches);
		$this->actionRequest = $matches[1];

	}


	public function colocarTrabalhadores($workers)
	{
		// Set-Cookie: PHPSESSID=ol67c0eqdvmrjsifsgjlln8kp2; path=/; HttpOnly
		// Set-Cookie: ikariam_loginMode=0
		// Set-Cookie: ikariam=20856_016da4864ca79459f96ea0a4ee6b3885; HttpOnly

		$host = 'https://s40-br.ikariam.gameforge.com/index.php';

		$url = $host;

		$actionRequest = $this->actionRequest;
		$cityId = $this->cityId;
		$islandId = $this->islandId;

		$data = "action=IslandScreen&actionRequest=".$actionRequest."&ajax=1&backgroundView=island&cityId=".$cityId."&currentIslandId=".$islandId."&function=workerPlan&islandId=".$islandId."&rw=".$workers."&screen=resource&templateView=resource&type=resource";

		echo $data;
		echo "<br><br>";

		$datalength = strlen($data);
		$header = array("Accept: s40-br.ikariam.gameforge.com", "Host: s40-br.ikariam.gameforge.com","Content-Type: application/x-www-form-urlencoded", "Conten-Length: $datalength");  

		$page = $this->curl($url, $this->cookies, $data, $header);

		// $page = str_replace("http://localhost:8080/php/bot_ikariam/bot.php", "https://s40-br.ikariam.gameforge.com/", $page);

		// $page = str_replace("http://localhost:8080/php/bot_ikariam/", "https://s40-br.ikariam.gameforge.com/", $page);

		// $page = str_replace("http://localhost:8080/", "https://s40-br.ikariam.gameforge.com/", $page);

		preg_match('/\"actionRequest\":\"(.*)\",/U', $page, $matches);
		$this->actionRequest = $matches[1];

		echo($page);

	}

	private function curl($url, $cookies, $post, $header = 1, $json = 0, $ref = 0, $xml = 0)
	{
		$ch = @curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if ($json == 1) {
			$head[] = "Content-type: application/json";
			$head[] = "X-Requested-With: XMLHttpRequest";
		}
		if ($xml == 1) {
			$head[] = "X-Requested-With: XMLHttpRequest";
		}
		$head[] = "Connection: keep-alive";
		$head[] = "Keep-Alive: 300";
		$head[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
		$head[] = "Accept-Language: en-us,en;q=0.5";
		if ($cookies) curl_setopt($ch, CURLOPT_COOKIE, $cookies);

		curl_setopt($ch, CURLOPT_REFERER, $ref == 0 ? $url : $ref);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
		if($header == -1){
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_NOBODY, 1);
		}
		else curl_setopt($ch, CURLOPT_HEADER, $header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if ($post) {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		$loginpassw = 'bot:bot';
    	$proxy_ip = '127.0.0.1';
    	$proxy_port = '9050';

		curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
    	curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
	    curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
    	curl_setopt($ch, CURLOPT_PROXYUSERPWD, $loginpassw);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		$page = curl_exec($ch);
		curl_close($ch);
		return $page;
	}

	private function GetCookies($content)
	{
		preg_match_all('/Set-Cookie: (.*);/U',$content,$temp);
		$cookie = $temp[1];
		$cookies = "";
		$a = array();
		foreach($cookie as $c){
			$pos = strpos($c, "=");
			$key = substr($c, 0, $pos);
			$val = substr($c, $pos+1);
			$a[$key] = $val;
		}
		foreach($a as $b => $c){
			$cookies .= "{$b}={$c}; ";
		}
		return $cookies;
	}


};

?>