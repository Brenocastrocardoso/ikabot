<?php

require_once "global.php";

Class Telefone extends Comum implements JsonSerializable
{
    const DIGITOS_DDD = 2;
    const DIGITOS_TELEFONE_MAX = 9;
    const DIGITOS_TELEFONE_MIN = 8;

    /**
     * @var void
     */
    private $numero;
    
    /**
     * @var void
     */
    private $ddd;

    public function Telefone($nnumero, $nddd)
    {
        $this->setNumero($nnumero);
        $this->setDDD($nddd);
    }

    public function notNullGenerator()
    {
        yield $this->numero;
        yield $this->ddd;
    }
    public function notNullShowable()
    {
        yield TRUE;       
    }


    public function setDDD($nddd): bool
    {

        $nddd = clearInput($nddd);
        if (!empty($nddd))
        {
            if (preg_match('/^[0-9]{'.Telefone::DIGITOS_DDD.'}$/',$nddd))
            {
                $this->ddd = $nddd;

                return TRUE;
            }
            
        }
        return FALSE;
    }

    public function setNumero($nnumero)
    {
        $nnumero = clearInput($nnumero); 
        if (!empty($nnumero))
        {
            if (preg_match('/^[0-9]{'.Telefone::DIGITOS_TELEFONE_MIN.','.Telefone::DIGITOS_TELEFONE_MAX.'}$/',$nnumero))
            {
                $this->numero = $nnumero;
            }
        }
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function getDDD()
    {
        return $this->ddd;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

}