<?php

require_once "global.php";
require_once "pessoa.class.php";
/**
 *
 */
class Debito extends Comum implements JsonSerializable
{

    const STATUS = array("Ativo", "Cancelado");

    /**
     *
     */
    public function Debito($nCredor=NULL, $nDevedor=NULL, $nValor=NULL, $nAddData=NULL, $nDescricao=NULL, $nStatus=NULL, $nid=NULL)
    {
        $this->setCredor($nCredor);
        $this->setDevedor($nDevedor);
        $this->setValor($nValor);
        $this->setAddData($nAddData);
        $this->setDescricao($nDescricao);
        $this->setStatus($nStatus);
        $this->setId($nid);

    }

    /**
     * @var void
     */
    private $valor;

    /**
     * @var void
     */
    private $status;

    /**
     * @var void
     */
    private $addData;

    /**
     * @var void
     */
    private $descricao;

    /**
     * @var void
     */
    private $credor;

    /**
     * @var void
     */
    private $devedor;
    /**
     * @var void
     */
    private $id;


    public function notNullGenerator()
    {
        yield $this->valor;
        yield $this->addData;
        yield $this->status;
        yield $this->credor;
        yield $this->devedor;
    }

    public function notNullShowable()
    {
        yield $this->valor;
        yield $this->addData;
        yield $this->status;
        yield $this->id;
    }

    public function jsonSerialize()
    {
        $vars = array(
                "valor"=>$this->valor,
                "addData"=>$this->addData,
                "status"=>$this->status,
                "id"=>$this->id,
                "descricao"=>$this->descricao
          );

        return $vars;
    }

    public function setCredor($nCredor):bool
    {
        if (($nCredor instanceof Identidade) && !($nCredor instanceof Pessoa) && $nCredor->isValid())
        {
            $this->credor = $nCredor;
            return TRUE;
        }
        return FALSE;
    }

    
    public function setDevedor($nDevedor):bool
    {
        if (($nDevedor instanceof Identidade) && !($nDevedor instanceof Pessoa) && $nDevedor->isValid())
        {
            $this->devedor = $nDevedor;
            return TRUE;
        }
        return FALSE;
    }

    
    public function setValor($nValor):bool
    {
        if (!empty($nValor))
        {
            if (preg_match('/^-{0,1}[0-9]{1,5}.[0-9]{2}/',$nValor))
            {
                $this->valor = $nValor;
                return TRUE;
            }
        }
        return FALSE;
    }


    public function setAddData($nAddData):bool
    {
        if (!empty($nAddData))
        {
            if (preg_match('/^20[0-9]{2}-[0-9]{2}-[0-9]{2}/',$nAddData))
            {
                $this->addData = $nAddData;
                return TRUE;
            }
        }
        return FALSE;   
    }
    public function setId($id): bool
    {
        if (!empty($id))
        {
            if (preg_match('/^[0-9]*$/',$id))
            {
                $this->id = $id;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function setDescricao($nDescricao):bool
    {
        $nDescricao = clearInput($nDescricao);
        if (!empty($nDescricao)){

            if (preg_match('/^[a-zA-Z ]*$/',$nDescricao))
            {
                $this->descricao = $nDescricao;
                return TRUE;
            }
        }
        return FALSE;
    }


    public function setStatus($nStatus):bool
    {
        foreach (Debito::STATUS as $i) {
            if ($nStatus==$i)
            {
                $this->status = $nStatus;
                return TRUE;
            }
        }
        return FALSE;            
    }

    public function getCredor()
    {
        return $this->credor;
    }

    public function getDevedor()
    {
        return $this->devedor;
    }

    public function getValor()
    {
        return $this->valor;
    }

    public function getAddData()
    {
        return $this->addData;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function getStatus()
    {
        return $this->status;
    }
    public function getId()
    {
        return $this->id;
    }

}
