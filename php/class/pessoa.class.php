<?php

require_once "global.php";
require_once "conta.class.php";
require_once "telefone.class.php";
require_once "fatura.class.php";
require_once "debito.class.php";
require_once "chamado.class.php";
require_once "db.class.php";
require_once "endereco.class.php";
require_once "predio.class.php";

/**
 *Classe implementada criar uma interface comum a todas as pessoas do sistema
 */
class Identidade extends Comum implements JsonSerializable
{

    const ESTADO_CIVIL = array("solteiro (a)", "casado (a)", "divorciado (a)", "viúvo (a)", "separado (a)");
    const STATUS = array('Advogado', 'Administrador', 'Manutenção', 'Locatario', 'Locador');


    /**
     * @var void
     */
    private $senha;

    /**
     * @var void
     */
    private $nome;

    /**
     * @var void
     */
    private $email;

    /**
     * @var void
     */
    private $cpf;

    /**
     * @var void
     */
    private $rg;
    /**
     * @var void
     */
    private $status;
    /**
     * @var void
     */
    private $nacionalidade;
    /**
     * @var void
     */
    private $estadoCivil;
    /**
     * @var void
     */
    private $id;



    const DIGITOS_RG_MAX = 13;
    const DIGITOS_RG_MIN = 6;
    const DIGITOS_CPF = 11;


    /**
     * @param void $login
     * @param void $senha
     */
    // public function login($login, $senha):void
    // {
        // TODO: implement here
    // }

    // public function gerarRelatorio()
    // {

    // }

//    public function Identidade(){}

    public function notNullGenerator()
    {
        yield $this->senha;
        yield $this->cpf;
    }
    public function notNullShowable()
    {
        yield $this->cpf;
    }

    public function setId($id):bool
    {
        if (!empty($id))
        {    
            if (preg_match('/^[0-9]*$/',$id))
            {
                $this->id = $id;
                return TRUE;
            }
        }
        return FALSE;
    }


    public function setRg($nrg):bool
    {
        $nrg = clearInput($nrg);
        if (!empty($nrg))
        {    
            if (preg_match('/^[0-9]{'.Pessoa::DIGITOS_RG_MIN.','.Pessoa::DIGITOS_RG_MAX.'}$/',$nrg))
            {
                $this->rg = $nrg;
                return TRUE;
            }
        }
        return FALSE;  
    }

    /** Valida e modifica o atributo
    *@param $nome
    */
    public function setNome($nnome):bool
    {
        $nnome = clearInput($nnome);
        if (!empty($nnome))
        {    
            if (preg_match('/^[a-zA-Z ]*$/',$nnome))
            {
                $this->nome = $nnome;
                return TRUE;
            }
        }
        return FALSE;
    }


    /** Valida e modifica o atributo
     * @param void $senha
     */
    public function setSenha($nsenha):bool
    {
        $nsenha = clearInput($nsenha);
        if (!empty($nsenha))
        {            
            if (preg_match('/^[a-zA-Z0-9_@!#%&*()-+.,<> ]{6}[a-zA-Z0-9_@!#%&*()-+.,<> ]*$/',$nsenha))
            {
                $this->senha = $nsenha;
                return TRUE;
            }
        }
        return FALSE;
    }

    /** Valida e modifica o atributo
     * @param void $email
     */
    public function setEmail($nemail):bool
    {
        $nemail = clearInput($nemail);
        if (!empty($nemail))
        {            
            if (filter_var($nemail, FILTER_VALIDATE_EMAIL))
            {
                $this->email = $nemail;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function inserirDb():bool
    {
        $db = new Db();
        //var_dump($this);
        if ($this->isValid())
            return ($ret = $db->tryInsert("Pessoas", [
                                        "pessoas_senha" => $this->getSenha(),
                                        "pessoas_cpf" => $this->getCpf(),
                                        "pessoas_nacionalidade" => $this->getNacionalidade(),
                                        "pessoas_estado_civil" => $this->getEstadoCivil(),
                                        "pessoas_nome" => $this->getNome(),
                                        "pessoas_email" => $this->getEmail(),
                                        "pessoas_rg" => $this->getRg()
                                        ], ["pessoas_cpf" => $this->getCpf()]));
        return FALSE;
    }
    public function setNacionalidade($nnacionalidade):bool
    {
        $nnacionalidade = clearInput($nnacionalidade);
        if (!empty($nnacionalidade))
        {            
            if (validarString($nnacionalidade))
            {
                $this->nacionalidade = $nnacionalidade;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function setEstadoCivil($nStatus): bool
    {
        foreach (Identidade::ESTADO_CIVIL as $i) {
            if ($nStatus==$i)
            {
                $this->estadoCivil = $nStatus;
                return TRUE;
            }
        }
        return FALSE; 
    }
    public function setStatus($nStatus): bool
    {
        foreach (Identidade::STATUS as $i) {
            if ($nStatus==$i)
            {
                $this->status = $nStatus;
                return TRUE;
            }
        }
        return FALSE; 
    }
    

    /** Valida e modifica o atributo
     *@param $ncpf
     */
    public function setCpf($ncpf): bool
    {
        $ncpf = clearInput($ncpf);
        if (!empty($ncpf))
        {
            if (preg_match('/^[0-9]{'.Pessoa::DIGITOS_CPF.'}$/',$ncpf) && $this->cpf==NULL)
            {               
                $acumulador = 0;
                for($aux =10;$aux>1;$aux--)
                {
                    $acumulador += $ncpf[10-$aux]*$aux;
                }

                $resto = $acumulador%11;
                $resto < 2 ? $resto = 0 :  $resto = 11- $resto;

                if ($resto != $ncpf[9])
                    return FALSE;

                $acumulador = 0;
                for($aux =11;$aux>1;$aux--)
                {
                    $acumulador += $ncpf[11-$aux]*$aux;
                }

                $resto = $acumulador%11;
                $resto < 2 ? $resto = 0 :  $resto = 11- $resto;

                if ($resto != $ncpf[10])
                    return FALSE;

                $this->cpf = $ncpf;
                return TRUE;
            }
        }
        return FALSE;
    }


    /**Retorna o atributo
     *
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**Retorna o atributo
     *
     */
    public function getId()
    {
        return $this->id;
    }    
    public function getRg()
    {
        return $this->rg;
    }

    /**Retorna o atributo
     *
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**Retorna o atributo
     *
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**Retorna o atributo
     *
     */
    public function getNome()
    {
        return $this->nome;
    }    

    public function getNacionalidade()
    {
        return $this->nacionalidade;
    }

    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }
    public function getStatus()
    {
        return $this->status;
    }

    // public function __get($var)
    // {
    //     return $this->$var;
    // }

    public function jsonSerialize()
    {
        $vars = array("nome"=>$this->nome,
          "email"=>$this->email,
          "cpf"=>$this->cpf,
          "rg"=>$this->rg
          );

        return $vars;
    }

    protected function this()
    {
        return $this;
    }
}

class Pessoa extends Identidade
{


    /**
     * @var void
     */
    private $telefones;

    /**
     * @var void
     */
    private $predios;
    /**
     * @var void
     */
    private $identidade;

    /**
     * @var void
     */
    private $faturas;

    /**
     * @var void
     */
    private $contas;

    /**
     * @var void
     */
    private $chamados;

    public function Pessoa($Identidade=NULL)
    {
        $this->setIdentidade($Identidade);
    }

    public function login():bool
    {
        $db = new Db();
        
        $raw_output = $db->search("pessoas", "*", array("pessoas_senha" => $this->identidade->getSenha(), "pessoas_cpf" => $this->identidade->getCpf()));
        if (!($pessoa = $raw_output->fetch()))
            return FALSE;


        $this->identidade->setNome($pessoa["pessoas_nome"]);
        $this->identidade->setRg($pessoa["pessoas_rg"]);
        $this->identidade->setEmail($pessoa["pessoas_email"]);
        $this->identidade->setId($pessoa["pessoas_id"]);
        $this->identidade->setStatus($pessoa["pessoas_status"]);
        $this->identidade->setEstadoCivil($pessoa["pessoas_estado_civil"]);
        $this->identidade->setNacionalidade($pessoa["pessoas_nacionalidade"]);
        
        // $this->identidade->setEstadoCivil($pessoa["pessoas_estado_civil"]);
        // $this->identidade->setNacionalidade($pessoa["pessoas_nacionalidade"]);
        // $this->identidade->setStatus($pessoa["pessoas_status"]);

        $_SESSION["Identidade"] = serialize($this->identidade);
        //var_dump($this->identidade);

        return TRUE;
    }

    public function carregarIdentidade()
    {
        $db = new Db();
        
        $raw_output = $db->search("pessoas", "*", array("pessoas_cpf" => $this->identidade->getCpf()));
        if (!($pessoa = $raw_output->fetch()))
            return FALSE;


        $this->identidade->setNome($pessoa["pessoas_nome"]);
        $this->identidade->setRg($pessoa["pessoas_rg"]);
        $this->identidade->setEmail($pessoa["pessoas_email"]);
        $this->identidade->setId($pessoa["pessoas_id"]);
        $this->identidade->setStatus($pessoa["pessoas_status"]);
        $this->identidade->setEstadoCivil($pessoa["pessoas_estado_civil"]);
        $this->identidade->setNacionalidade($pessoa["pessoas_nacionalidade"]);
        

        return TRUE;
    }

    public function carregarDebitos($fatura)
    {
         $db = new Db();

        $prep = Db::$instance->prepare
        (
            "SELECT 
            DEB.debitos_valor,
            DEB.debitos_id,
            DEB.debitos_add_data, 
            DEB.debitos_descricao,
            DEB.debitos_status
            FROM(

            SELECT 
            debitos.debitos_valor*-1 AS debitos_valor,
            debitos.debitos_id,
            debitos.debitos_add_data, 
            debitos.debitos_descricao,
            debitos.debitos_status
            FROM `faturas`
            INNER JOIN `debitos` ON (debitos.faturas_id_credor = faturas.faturas_id)
            WHERE faturas.faturas_id = :id

            UNION

            SELECT 
            debitos.debitos_valor,
            debitos.debitos_id,
            debitos.debitos_add_data, 
            debitos.debitos_descricao,
            debitos.debitos_status
            FROM `faturas`
            INNER JOIN `debitos` ON (debitos.faturas_id_devedor = faturas.faturas_id)
            WHERE faturas.faturas_id = :id
            )DEB"
        );
        $prep->bindValue(":id",$fatura->getId());
        $prep->execute();
        $output = $prep->fetchAll();
        foreach ($output as $i)
        {
    // public function Debito($nCredor=NULL, $nDevedor=NULL, $nValor=NULL, $nAddData=NULL, $nDescricao=NULL, $nStatus=NULL, $nid=NULL)
            if ($i["debitos_valor"]<0){
                $deb = new Debito(
                              $this->getIdentidade(),
                              NULL,
                              $i["debitos_valor"],
                              $i["debitos_add_data"],
                              $i["debitos_descricao"],
                              $i["debitos_status"],
                              $i["debitos_id"]
                              );
            }else{
                $deb = new Debito(
                              NULL,
                              $this->getIdentidade(),
                              $i["debitos_valor"],
                              $i["debitos_add_data"],
                              $i["debitos_descricao"],
                              $i["debitos_status"],
                              $i["debitos_id"]
                              );
            }

            // var_dump($deb);
            if ($deb->isShowable())
                $fatura->addDebito($deb);

        }
    }

    public function carregarFaturas($tudo = false)
    {       
        $db = new Db();

        $prep = Db::$instance->prepare(
"SELECT 
    FAT.faturas_id,
    FAT.faturas_vencimento,
    CONVERT(SUM(FAT.valor_taxa)*-1, DECIMAL(10,2)) AS valor_taxa,
    CONVERT((SUM(FAT.valor_original)-SUM(FAT.valor_taxa))*FAT.faturas_juros/100*DATEDIFF(NOW(),FAT.faturas_vencimento)*(DATEDIFF(NOW(),FAT.faturas_vencimento)>0), DECIMAL(10,2)) AS valor_juros,
    CONVERT(((SUM(FAT.valor_original)-SUM(FAT.valor_taxa))*FAT.faturas_multa/100)*(DATEDIFF(NOW(),FAT.faturas_vencimento)>0), DECIMAL(10,2)) AS valor_multa, 
    FAT.faturas_status, 
    FAT.faturas_boleto_nr,
    CONVERT(SUM(FAT.valor_fatura), DECIMAL(10,2)) AS valor_total 
FROM(

    SELECT faturas.faturas_id, faturas.faturas_vencimento, faturas.faturas_juros, faturas.faturas_multa, faturas.faturas_status, faturas.faturas_boleto_nr,SUM(debitos.debitos_valor*-1) AS valor_original,SUM(debitos.debitos_valor*-1)*0.1 AS valor_taxa, (SUM(debitos.debitos_valor*-0.9) *((1 + faturas.faturas_multa/100 + DATEDIFF(NOW(), faturas.faturas_vencimento)*faturas.faturas_juros/100))) AS valor_fatura FROM `debitos` 
INNER JOIN `faturas` ON (debitos.faturas_id_credor = faturas.faturas_id) 
WHERE faturas.faturas_vencimento < NOW() AND debitos.pessoas_id_credor = :id
GROUP BY faturas.faturas_id

UNION

SELECT faturas.faturas_id, faturas.faturas_vencimento, faturas.faturas_juros, faturas.faturas_multa, faturas.faturas_status, faturas.faturas_boleto_nr,SUM(debitos.debitos_valor*-1) AS valor_original,SUM(debitos.debitos_valor*-1)*0.1 AS valor_taxa, SUM(debitos.debitos_valor*-0.9) AS valor_fatura FROM `debitos` 
INNER JOIN `faturas` ON (debitos.faturas_id_credor = faturas.faturas_id)
WHERE faturas.faturas_vencimento >= NOW() AND debitos.pessoas_id_credor = :id
GROUP BY faturas.faturas_id

UNION

SELECT faturas.faturas_id, faturas.faturas_vencimento, faturas.faturas_juros, faturas.faturas_multa, faturas.faturas_status, faturas.faturas_boleto_nr,SUM(debitos.debitos_valor) AS valor_original, 0.00 AS valor_taxa, (SUM(debitos.debitos_valor) *((1 + faturas.faturas_multa/100 + DATEDIFF(NOW(), faturas.faturas_vencimento)*faturas.faturas_juros/100))) AS valor_fatura FROM `debitos` 
INNER JOIN `faturas` ON (debitos.faturas_id_devedor = faturas.faturas_id) 
WHERE faturas.faturas_vencimento < NOW() AND debitos.pessoas_id_devedor = :id
GROUP BY faturas.faturas_id

UNION

SELECT faturas.faturas_id, faturas.faturas_vencimento, faturas.faturas_juros, faturas.faturas_multa, faturas.faturas_status, faturas.faturas_boleto_nr, SUM(debitos.debitos_valor) AS valor_original,0.00 AS valor_taxa, SUM(debitos.debitos_valor) AS valor_fatura FROM `debitos` 
INNER JOIN `faturas` ON (debitos.faturas_id_devedor = faturas.faturas_id)
WHERE faturas.faturas_vencimento >= NOW() AND debitos.pessoas_id_devedor = :id) FAT
GROUP BY FAT.faturas_id"

);
        $prep->bindValue(":id",$this->getIdentidade()->getId());
        $prep->execute();
        $output = $prep->fetchAll();
        foreach ($output as $i)
        {
 //public function Fatura($vencimento=NULL, $status=NULL, $id=NULL, $valor=NULL, $taxa=NULL, $boleto=NULL, $juros=NULL, $multa=NULL)
            $fat = new Fatura($i["faturas_vencimento"],
                              $i["faturas_status"],
                              $i["faturas_id"],
                              $i["valor_total"],
                              $i["valor_taxa"],
                              $i["faturas_boleto_nr"],
                              $i["valor_juros"],
                              $i["valor_multa"]
                              );
            if ($fat->isShowable()){
                $this->addFatura($fat);

                if($tudo)
                    $this->carregarDebitos($fat);
            }

        }
    }

    public function procurarFatura($id)
    {
        if ($this->faturas==NULL)
            return FALSE;
        foreach ($this->faturas as $i)
        {
            if ($i->getId() == $id)
                return $i;
        }
        return FALSE;
    }

    public function procurarIdentidade($id)
    {
        $db = new Db();
        
        $raw_output = $db->search("pessoas", "*", array("pessoas_id" => $id));
        if (!($pessoa = $raw_output->fetch()))
            return FALSE;

        $ident = new Identidade();
        $ident->setCpf($pessoa["pessoas_cpf"]);
        $ident->setNome($pessoa["pessoas_nome"]);
        $ident->setRg($pessoa["pessoas_rg"]);
        $ident->setEmail($pessoa["pessoas_email"]);
        $ident->setId($pessoa["pessoas_id"]);

        //echo "ident isValid: " . (int)$ident->isValid() . "done <br><br>";
        
        // $this->$ident->setEstadoCivil($pessoa["pessoas_estado_civil"]);
        // $this->$ident->setNacionalidade($pessoa["pessoas_nacionalidade"]);
        // $this->$ident->setStatus($pessoa["pessoas_status"]);
        //var_dump($this->$ident);

        return $ident;
    }



    public function carregarPredios()
    {
         $db = new Db();

        $prep = Db::$instance->prepare
        (
            "SELECT
            estados.estados_nome,
            estados.estados_id,
            predios.predios_un_celesc,
            predios.predios_id_aguas,
            predios.predios_nome,
            predios.predios_id,
            ruas.ruas_nome,
            ruas.ruas_id,
            bairros.bairros_nome,
            bairros.bairros_id,
            cidades.cidades_nome,
            cidades.cidades_id
            FROM `predios` 
            INNER JOIN `apartamentos` ON (apartamentos.predios_id = predios.predios_id)
            INNER JOIN `ruas` ON (ruas.ruas_id = predios.ruas_ruas_id)
            INNER JOIN `bairros` ON (ruas.bairros_id = bairros.bairros_id)
            INNER JOIN `cidades` ON (bairros.cidades_id = cidades.cidades_id)
            INNER JOIN `estados` ON (cidades.estados_id = estados.estados_id)
            WHERE apartamentos.pessoas_id_proprietario = :id
            GROUP BY predios.predios_id"
        );
        $prep->bindValue(":id",$this->getIdentidade()->getId());
        $prep->execute();
        $output = $prep->fetchAll();
        //var_dump($output);
        foreach ($output as $i)
        {
    // public function Endereco($estado, $cidade, $bairo, $rua, $idestado, $idcidade, $idbairo, $idrua)
            $end = new Endereco(
                                $i["estados_nome"],
                                $i["cidades_nome"],
                                $i["bairros_nome"],
                                $i["ruas_nome"],
                                $i["estados_id"],
                                $i["cidades_id"],
                                $i["bairros_id"],
                                $i["ruas_id"]
                            );
            //var_dump($end);
            //var_dump($i["bairros_nome"]);
            if ($end->isShowable())
            {

            // public function Predio($id, $endereco, $cadastroCelesc, $cadastroAgua)
                $pre = new Predio(
                                  $i["predios_id"],
                                  $end,
                                  $i["predios_un_celesc"],
                                  $i["predios_id_aguas"]
                                 );
                if ($pre->isShowable())
                {
                    $this->carregarApartamentos($pre);
                    $this->addPredio($pre);
                }
            }

            
        }
    }
    
public function carregarApartamentos($predio)
        {
            /*SELECT * FROM `predios` 
    INNER JOIN apartamentos ON (apartamentos.predios_id = predios.predios_id)
    WHERE apartamentos.pessoas_id_proprietario = 1 AND predios.predios_id = 1*/
            $db = new Db();

            $prep = Db::$instance->prepare
            (
                "SELECT * FROM `predios` 
                INNER JOIN apartamentos ON (apartamentos.predios_id = predios.predios_id)
                WHERE apartamentos.pessoas_id_proprietario = :pessoaid AND predios.predios_id = :predioid"
            );
            $prep->bindValue(":predioid",$predio->getId());
            $prep->bindValue(":pessoaid",$this->getIdentidade()->getId());
            $prep->execute();
            $output = $prep->fetchAll();
            //var_dump($output);
            foreach ($output as $i)
            {
// public function Apartamento($numero, $contrato, $predio)
                    $apto = new Apartamento(
                                  $i["apartamentos_nr"]
                                  );
                   
                //var_dump($apto);
            if ($apto->isShowable()){
                $predio->addApartamento($apto);
            }
            }


            
        }

    public function carregarChamados()
    {
        $db = new Db();

        $raw_output = $db->search("chamado", "*", array("pessoas_id" => $this->identidade->getId()));
        $output = $raw_output->fetchAll();
        foreach ($output as $i)
        {
//    public function Chamado($criador=NULL, $revisor=NULL, $descricao=NULL, $status=NULL, $addData=NULL, $assunto=NULL, $closeData=NULL)
        //var_dump($i["chamado_descricao"]);
            $cham = new Chamado(
                $this->getIdentidade(),
                $this->procurarIdentidade(
                $i["pessoas_id_revisor"]),
                $i["chamado_descricao"],
                $i["chamado_status"],
                $i["chamado_add_data"],
                $i["chamado_assunto"],
                $i["chamado_close_data"]
            );
            // echo (int)$cham->isValid();
            $this->addChamados($cham);
        //    var_dump($cham);
        }
    }

    public function gerarRelatorio()
    {

    }

    /** Valida e modifica o atributo
     * @param void $telefone
     */
    public function addTelefone($ntelefone): bool
    {
        if (($ntelefone instanceof Telefone) && $ntelefone->isValid())
        {
            if ($this->telefones == NULL)
                $this->telefones = array();
            array_push($this->telefones, $ntelefone);
            return TRUE;
        }
        return FALSE;

    }

    public function notNullGenerator()
    {
        yield $this->identidade;
    }

    public function notNullShowable()
    {
        yield TRUE;       
    }

    /** Valida e modifica o atributo
    *@param $nconta
    */
    public function setIdentidade($nidentidade):bool
    {
        if (($nidentidade instanceof Identidade) && $nidentidade->isValid())
        {
            $this->identidade = $nidentidade;
            return TRUE;
        }
        return FALSE;
    }

    /** Valida e modifica o atributo
    *@param $nconta
    */
    public function addConta($ncontas):bool
    {
        if (($ncontas instanceof Conta) && $ncontas->isValid())
        {
            if ($this->contas == NULL)
                $this->contas = array();
            array_push($this->contas, $ncontas);
            return TRUE;
        }
        return FALSE;
    }

    /** Valida e modifica o atributo
    *@param $nconta
    */
    public function addChamados($nchamados):bool
    {
        if (($nchamados instanceof Chamado) && $nchamados->isValid())
        {
            //echo "chamado validado";
            if ($this->chamados == NULL)
                $this->chamados = array();
            array_push($this->chamados, $nchamados);
            return TRUE;
        }
        return FALSE;
    }

    /** Valida e modifica o atributo
    *@param $ndebitos
    */
    public function addFatura($nfatura):bool
    {
        if (($nfatura instanceof Fatura) && $nfatura->isValid())
        {
            if ($this->faturas == NULL)
                $this->faturas = array();
            array_push($this->faturas, $nfatura);
            return TRUE;
        }
        return FALSE;
    }

    /** Valida e modifica o atributo
    *@param $napartamento
    */
    public function addPredio($nPredio):bool
    {
        if (($nPredio instanceof Predio) && $nPredio->isValid())
        {
            if ($this->predios == NULL)
                $this->predios = array();
            array_push($this->predios, $nPredio);
            return TRUE;
        }
        return FALSE;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

    /** Valida e modifica o atributo
    *@param $napartamento
    */
    public function getFaturas()
    {
        return $this->faturas;
    }

    /** Valida e modifica o atributo
    *@param $napartamento
    */
    public function getPredios()
    {
        return $this->predios;
    }

    /** Valida e modifica o atributo
    *@param $napartamento
    */
    public function getContas()
    {
        return $this->contas;
    }

    /** Valida e modifica o atributo
    *@param $napartamento
    */
    public function getChamados()
    {
        return $this->chamados;
    }

    /** Valida e modifica o atributo
    *@param $napartamento
    */
    public function getIdentidade()
    {
        return $this->identidade;
    }


    /**Retorna o atributo
     *
     */
    public function getTelefone()
    {
        return $this->telefones;
    }    

}