<?php
require_once "global.php";
require_once "predio.class.php";

/**
 *
 */
class Apartamento extends Comum implements JsonSerializable
{

    public function Apartamento($numero=NULL, $contrato=NULL, $predio=NULL)
    {
        $this->setNumero($numero);
        $this->setContrato($contrato);
        $this->setPredio($predio);
    }

    /**
     * @var void
     */
    // public $cadastroCelesc;

    /**
     * @var void
     */
    // public $end_numero;

    /**
     * @var void
     */
    public $numero;

    /**
     * @var void
     */
    // public $locador;

    /**
     * @var void
     */
    public $contrato;

    /**
     * @var void
     */
    public $predio;

    /**
     * @var void
     */
    // public $rua;



    /**
     *
     */
    // public function getCadastroCelesc(){return $this->}

    // public function getCadastroAgua(){return $this->}

    // public function getLocatario(){return $this->locatario;}

    // public function getLocador(){return $this->locador;}

    // public function setLocador($nidentidade):bool
    // {
    //     if (($nidentidade instanceof Identidade) && $nidentidade->isValid())
    //     {
    //         $this->locador = $nidentidade;
    //         return TRUE;
    //     }
    //     return FALSE;
    // }

    // public function setRua($nrua):bool
    // {
    //     if (($nrua instanceof Rua) && $nrua->isValid())
    //     {
    //         $this->rua = $nrua;
    //         return TRUE;
    //     }
    //     return FALSE;
    // }


    // public function setLocatario($nidentidade):bool
    // {
    //     if (($nidentidade instanceof Identidade) && $nidentidade->isValid())
    //     {
    //         $this->lcoatario = $nidentidade;
    //         return TRUE;
    //     }
    //     return FALSE;
    // }    

    public function setpredio($npredio):bool
    {
        if (($npredio instanceof Predio) && $npredio->isShowable())
        {
            $this->predio = $npredio;
            return TRUE;
        }
        return FALSE;
    }
    public function setNumero($nnumero):bool
    {
         if (!empty($nnumero))
        {
            if (preg_match('/^[0-9]*$/',$nnumero))
            {
                $this->numero = $nnumero;
                return TRUE;
            }
        }
        return FALSE;
    }
    public function setContrato($ncontrato):bool
    {
        if (($ncontrato instanceof Contrato) && $ncontrato->isShowable())
        {
            $this->contrato = $ncontrato;
            return TRUE;
        }
        return FALSE;
    }

    // public function setCadastroCelesc():bool
    // {
    // }

    // public function setCadastroAgua():bool
    // {
    // }

    // public function gerarContrato():bool
    // {
    // }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

    public function notNullGenerator()
    {
        yield $this->numero;
    }

    public function notNullShowable()
    {
        yield $this->numero;       
    }

}
