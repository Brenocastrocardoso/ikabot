<?php

require_once "global.php";

Class Banco extends Comum implements JsonSerializable
{

    /**
     * @var void
     */
    private $nome;

    public function Banco($nnome)
    {
    	$this->setNome($nnome);
    }

    public function setNome($nnome)
    {
    	$nnome = clearInput($nnome);
        if (!empty($nnome))
        {    
            if (preg_match('/^[a-zA-Z ]*$/',$nnome))
            {
                $this->nome = $nnome;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getNome()
    {
    	return $this->nome;
    }

    public function notNullGenerator()
    {
        yield $this->nome;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

}
