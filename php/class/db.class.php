<?php
//error_reporting(0);
/**
* @brief Arquivo de abstração das funções do banco de dados
* @file db.class.php
*/


/**
* @brief Classe que faz a conexão com o banco de dados
*/
class Db
{

	/**
	* @brief Variavel que guarda a conexão com o banco de dados.
	*/
	public static $instance;

	/**
	* @brief Construtor da classe Db, ele efetua a conexão com o banco de dados, chamando as configurações do arquivo config.php
	*/
	function __construct()
	{  
		if (!isset(self::$instance)) 
		{
			require_once("../config.php");
			self::$instance = new PDO(DB_TIPO.":host=".DB_HOST.";dbname=".DB_BANCO, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
		}
	}

	/**
	* @brief Função pública que executa uma query no banco de dados
	* @param sql é a query a ser executada
	* @return Retorna o resultado da query
	*/
	public static function executeQuery($sql)
	{
		return self::$instance->query($sql);
	}

	/**
	* @brief Função que monta uma query SELECT e executa no banco de dados
	* @param $from é o nome da tabela e os joins (caso necessário)
	* @param $camposDesejados, tem como padrão todos, mas se você quiser, pode filtrar os campos que deseja retornar
	* @param $array_where por padrão é null, você pode mandar uma string ou um array, a string tem que vir com a condição, o array tem que ser por exemplo $array_where[coluna] = valor para condições que sejam "AND"
	* @return Retorna o resultado da query
	*/
	public static function search($from, $camposDesejados = "*", $array_where = null)
	{

		require_once("defines.php");

		$camposDesejados = clearInput($camposDesejados);
		$from = clearInput($from);

		$sql = "SELECT $camposDesejados FROM $from";

		if(gettype($array_where) == "array")
		{
			$aux = 1; // Utilizado para colocar a correspondente

			$sql .= " WHERE ";

			foreach ($array_where as $key => $value) 
			{
				$sql .= $key." = ? AND ";
			}

			$sql = substr($sql, 0, -4);
			
			$stmt = self::$instance->prepare($sql);

			foreach ($array_where as $key => $value) 
			{
				$stmt->bindValue($aux,  clearInput($value));
				$aux++;
			}

		}
		else if($array_where != null)
		{
			$sql .= " WHERE ?";
			$stmt = self::$instance->prepare($sql);
			$stmt->bindValue(1, clearInput($array_where));
			
		}	
		else
			$stmt = self::$instance->prepare($sql);	


		$stmt->execute();

		return $stmt;

	}

	/**
	* @brief Função que monta uma query INSERT e executa no banco de dados
	* @param $from é o nome da tabela
	* @param $dados, é um array associativo que receberá $nomeDoArray['nomeDaTabela'] = valorAInserir
	* @return Retorna TRUE para caso haja sucesso na inserção e FALSE para caso tenha havido falha
	*/
	public static function insert($from, $dados)
	{

		require_once("defines.php");
		$value_insert = null;

		$from = clearInput($from);

		$sql = "INSERT INTO $from (";

		foreach ($dados as $key => $value) 
		{
			$sql          .= $key.", ";
			$value_insert .= ":".$key.", ";
		}
		
		$sql = substr($sql, 0, -2);
		$value_insert = substr($value_insert, 0, -2);
		$sql .= ") VALUES (".$value_insert.")";

		$stmt = self::$instance->prepare($sql);

		foreach ($dados as $key => $value) 
		{
			$stmt->bindValue(":".$key,  clearInput($value));
		}
		$stmt->execute();
		return $stmt->rowCount();
	}

	public static function tryInsert($from, $dados, $notRep)
	{
		require_once("defines.php");

		echo "###############TENTANDO INSERIR";

		$value_insert = null;

		$from = clearInput($from);

		$sql = "INSERT INTO $from (";

		foreach ($dados as $key => $value) 
		{
			$sql          .= $key.", ";
			$value_insert .= ":".$key.", ";
		}
		
		$sql = substr($sql, 0, -2);
		$value_insert = substr($value_insert, 0, -2);
		$sql .= ") SELECT ".$value_insert . " ";
		$sql .= "FROM DUAL WHERE NOT EXISTS (SELECT ";

		foreach ($notRep as $key => $value) 
		{
			$sql          .= $key.", ";
		}
		$sql = substr($sql, 0, -2);
		$sql .= " FROM ";
		$sql .= " $from ";
		$sql .= " WHERE ";

		$size = count($notRep);
		$i=0;
		foreach ($notRep as $key => $value) 
		{
			$sql          .= $key."  = :";
			$sql .= ($key. " ");
			$i++;
			if ($i < $size)
				$sql .= "AND ";
		}
		//$sql = substr($sql, 0, -2);
//		$value_insert = substr($value_insert, 0, -2);
		$sql .= ")";

		$stmt = self::$instance->prepare($sql);

		foreach ($dados as $key => $value) 
		{
			$stmt->bindValue(":".$key,  clearInput($value));
		}

		foreach ($notRep as $key => $value) 
		{
			$stmt->bindValue(":".$key,  clearInput($value));
		}

		var_dump($sql);
		echo "<br>";
		echo "<br>";
		var_dump($stmt);

		//var_dump($sql);
		$stmt->execute();
		return $stmt->rowCount();	
	}

	// INSERT INTO Pessoas (pessoas_senha, pessoas_cpf, pessoas_nacionalidade, pessoas_estado_civil, pessoas_nome, pessoas_email, pessoas_rg) SELECT (:pessoas_senha, :pessoas_cpf, :pessoas_nacionalidade, :pessoas_estado_civil, :pessoas_nome, :pessoas_email, :pessoas_rg)FROM DUAL WHERE NOT EXISTS (SELECT pessoas_cpf FROM  Pessoas  WHERE pessoas_cpf  = :pessoas_cpf""
	// INSERT INTO Pessoas (pessoas.pessoas_cpf, pessoas.pessoas_senha)
 //    SELECT '07956020905', 123456 
 //    FROM DUAL
 //    WHERE NOT EXISTS(SELECT pessoas_cpf FROM pessoas WHERE pessoas_cpf = '07956020905')



	/**
	* @brief Função que monta uma query de UPDATE e executa no banco de dados
	* @param $from é o nome da tabela
	* @param $dados, é um array associativo que receberá $nomeDoArray['nomeDaTabela'] = valorEditar
	* @param $array_where por padrão é null, você pode mandar uma string ou um array, a string tem que vir com a condição, o array tem que ser por exemplo $array_where[coluna] = valor para condições que sejam "AND"
	* @return Retorna TRUE para caso haja sucesso na edição e FALSE para caso tenha havido falha
	*/
	public static function update($from, $dados, $array_where = null)
	{

		require_once("defines.php");
		$value_insert = null;

		$from = clearInput($from);

		$sql = "UPDATE $from SET ";

		foreach ($dados as $key => $value) 
		{
			$sql .= $key." = :".$key.", ";
		}
		
		$sql = substr($sql, 0, -2);


		if(gettype($array_where) == "array")
		{
			$sql .= " WHERE ";
			
			foreach ($array_where as $key => $value) 
			{
				$sql .= $key." = :".$key." AND ";
			}

			$sql = substr($sql, 0, -5);

			$stmt = self::$instance->prepare($sql);

			foreach ($array_where as $key => $value) 
			{
				$stmt->bindValue(":".$key,  clearInput($value));
			}

		}
		else if($array_where != null) 
		{
			
			$sql .= " WHERE ";

			$stmt = self::$instance->prepare($sql);
			$stmt->bindValue(1, clearInput($array_where));

		}
		else
			$stmt = self::$instance->prepare($sql);	

		foreach ($dados as $key => $value) 
		{
			$stmt->bindValue(":".$key,  clearInput($value));
		}

		// return $stmt->execute();
		return $sql;
	}

	/**
	* @brief Função que monta uma query de DELETE e executa no banco de dados
	* @param $from é o nome da tabela
	* @param $array_where por padrão é null, você pode mandar uma string ou um array, a string tem que vir com a condição, o array tem que ser por exemplo $array_where[coluna] = valor para condições que sejam "AND"
	* @return Retorna TRUE para caso haja sucesso na edição e FALSE para caso tenha havido falha
	*/
	public static function delete($from, $array_where = null){
		require_once("defines.php");

		$sql = "DELETE FROM $from";

		if(gettype($array_where) == "array")
		{
			$sql .= " WHERE ";
			
			foreach ($array_where as $key => $value) 
			{
				$sql .= $key." = :".$key." AND ";
			}

			$sql = substr($sql, 0, -4);

			$stmt = self::$instance->prepare($sql);

			foreach ($array_where as $key => $value) 
			{
				$stmt->bindValue(":".$key,  clearInput($value));
			}

		}
		else if($array_where != null) 
		{			
			$sql .= " WHERE ?";
			$stmt = self::$instance->prepare($sql);
			$stmt->bindValue(1, clearInput($array_where));

		}
		else
			$stmt = self::$instance->prepare($sql);	

		return $stmt->execute();

	}
}