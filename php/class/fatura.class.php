<?php

require_once "global.php";
require_once "pessoa.class.php";
require_once "debito.class.php";
/**
 *
 */
class Fatura extends Comum implements JsonSerializable
{
    const STATUS = array("Pago", "Aberto");

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

    public function Fatura($vencimento=NULL, $status=NULL, $id=NULL, $valor=NULL, $taxa=NULL, $boleto=NULL, $juros=NULL, $multa=NULL)
    {
        $this->setVencimento($vencimento);
        $this->setStatus($status);
        $this->setId($id);
        $this->setJuros($juros);
        $this->setMulta($multa);
        $this->setBoleto($boleto);
        $this->setValor($valor);
        $this->setTaxa($taxa);

    }

    /**
     * @var void
     */
    private $debitos;
    /**
     * @var void
     */
    private $vencimento;
    /**
     * @var void
     */
    private $status;
    /**
     * @var void
     */
    private $juros;
    /**
     * @var void
     */
    private $multa;
    /**
     * @var void
     */
    private $boleto;
    /**
     * @var void
     */
    private $id;
    /**
     * @var void
     */
    private $valor;
    /**
     * @var void
     */
    private $taxa;

    public function notNullGenerator()
    {
        yield $this->vencimento;
        yield $this->status;
    }

    public function notNullShowable()
    {
        yield $this->vencimento;
        yield $this->id;
        yield $this->status;
        if ($this->debitos!=NULL)
            yield $this->debitos;
        else
            yield $this->valor;
    }

    public function inserirDb():bool
    {
        $db = new Db();
        //var_dump($this);
        if ($this->isValid())
            return ($ret = $db->insert("faturas", [
                                        "faturas_vencimento" => $this->getVencimento(),
                                        "faturas_juros" => $this->getJuros(),
                                        "faturas_multa" => $this->getMulta(),
                                        "faturas_status" => $this->getStatus(),
                                        "faturas_boleto_nr" => $this->getBoleto()
                                        ]));
        return FALSE;
    }

    public function addDebito($ndebito)
    {
        if (($ndebito instanceof Debito) && $ndebito->isShowable())
        {
            if ($this->debitos == NULL)
                $this->debitos = new ArrayObject();
            $this->debitos->append($ndebito);
            return TRUE;
        }
        return FALSE;
    }

    public function setJuros($nJuros): bool
    {
        if (!empty($nJuros))
        {
            if (preg_match('/^[0-9]{1,5}.[0-9]{2,3}/',$nJuros))
            {
                $this->juros = $nJuros;
                return TRUE;
            }
        }
        return FALSE;
    }
    
    public function setVencimento($nvencimento): bool
    {
        if (!empty($nvencimento))
        {
            if (preg_match('/^20[0-9]{2}-[0-9]{2}-[0-9]{2}/',$nvencimento))
            {
                $this->vencimento = $nvencimento;
                return TRUE;
            }
        }
        return FALSE;   
    }
    
    public function setMulta($nmulta): bool
    {
        if (!empty($nmulta))
        {
            if (preg_match('/^[0-9]{1,5}.[0-9]{2,3}/',$nmulta))
            {
                $this->multa = $nmulta;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function setStatus($nStatus): bool
    {
        foreach (Fatura::STATUS as $i) {
            if ($nStatus==$i)
            {
                $this->status = $nStatus;
                return TRUE;
            }
        }
        return FALSE; 
    }

    public function setBoleto($nboleto): bool
    {
        if (!empty($nboleto))
        {
            if (preg_match('/^[0-9]{5}.[0-9]{5} [0-9]{5}.[0-9]{5} [0-9]{5}.[0-9]{5} [0-9] [0-9]{14}/',$nboleto))
            {
                $this->boleto = $nboleto;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function setId($id): bool
    {
        if (!empty($id))
        {
            if (preg_match('/^[0-9]*$/',$id))
            {
                $this->id = $id;
                return TRUE;
            }
        }
        return FALSE;
    }
    public function setValor($nValor):bool
    {
        if (!empty($nValor))
        {
            if (preg_match('/^-{0,1}[0-9]{1,5}.[0-9]{2}/',$nValor))
            {
                $this->valor = $nValor;
                return TRUE;
            }
        }
        return FALSE;
    }
    public function setTaxa($nValor):bool
    {
        if (!empty($nValor))
        {
            if (preg_match('/^[0-9]{1,5}.[0-9]{2}/',$nValor))
            {
                $this->taxa = $nValor;
                return TRUE;
            }
        }
        return FALSE;
    }


    public function getJuros()
    {
        return $this->juros;
    }
    
    public function getVencimento()
    {
        return $this->vencimento;
    }
    
    public function getMulta()
    {
        return $this->multa;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getBoleto()
    {
        return $this->boleto;
    }
    public function getId()
    {
        return $this->id;
    }
    public function getValor()
    {
        return $this->valor;
    }


}