<?php

require_once "global.php";

/**
 *
 */
class Chamado extends Comum implements JsonSerializable
{

    const STATUS = array("ENVIADO", "ACEITO_MANUTENCAO", "ACEITO_ADVOGADO", "RECUSADO", "PRONTO");

    /**
     *
     */
    public function Chamado($criador=NULL, $revisor=NULL, $descricao=NULL, $status=NULL, $addData=NULL, $assunto=NULL, $closeData=NULL)
    {     
        $this->setCriador($criador);
        $this->setRevisor($revisor);
        $this->setAddData($addData);
        $this->setCloseData($closeData);
        $this->setAssunto($assunto);
        $this->setDescricao($descricao);
        $this->setStatus($status);
    }

    /**
     * @var void
     */
    private $criador;

    /**
     * @var void
     */
    private $revisor;

    /**
     * @var void
     */
    private $descricao;

    /**
     * @var void
     */
    private $assunto;

    /**
     * @var void 
     */
    private $status;

    /**
     * @var void
     */
    private $addData;

    /**
     * @var void
     */
    private $closeData;

    public function notNullGenerator()
    {
        yield $this->criador;
        yield $this->revisor;
        yield $this->descricao;
        yield $this->status;
        yield $this->addData;
    }


    /**
     *
     */
    // public function enviarParaManuntencao():void
    // {
    //     // TODO: implement here
    // }

    /**
     *
     */
    public function setRevisor($nRevisor):bool
    {
        if (($nRevisor instanceof Identidade) && !($nRevisor instanceof Pessoa) && $nRevisor->isValid())
        {
            $this->revisor = $nRevisor;
            return TRUE;
        }
        return FALSE;
    }

    /**
     *
     */
    public function setStatus($nStatus):bool
    {
        foreach (Chamado::STATUS as $i) {
            if ($nStatus==$i)
            {
                $this->status = $nStatus;
                return TRUE;
            }
        }
        return FALSE;            
    }
    
    /**
     *
     */
    public function setCriador($nCriador):bool
    {
        if (($nCriador instanceof Identidade) && !($nCriador instanceof Pessoa) && $nCriador->isValid())
        {
            $this->criador = $nCriador;
            return TRUE;
        }
        return FALSE;
    }

    /**
     *
     */
    public function setDescricao($nDescricao):bool
    {
        $nDescricao = clearInput($nDescricao);
        if (validarString($nDescricao)){
            $this->descricao = $nDescricao;
            return TRUE;
        }
        return FALSE;
    }

    /**
     *
     */
    public function setAssunto($nAssunto):bool
    {
        $nAssunto = clearInput($nAssunto);
        if (validarString($nAssunto)){
            $this->assunto = $nAssunto;
            return TRUE;
        }
        return FALSE;
    }

    /**
     *
     */
    public function setAddData($nAddData):bool
    {
        $nAddData = clearInput($nAddData);
        if (validarData($nAddData)){
            $this->addData = $nAddData;
            return TRUE;
        }
        return FALSE;
    }

    /**
     *
     */
    public function setCloseData($nCloseData):bool
    {
        $nCloseData = clearInput($nCloseData);
        if (validarData($nCloseData)){
            $this->closeData = $nCloseData;
            return TRUE;
        }
        return FALSE;
    }

    public function getCriador(){return $this->criador;}
    public function getRevisor(){return $this->revisor;}
    public function getAddData(){return $this->addData;}
    public function getCloseData(){return $this->closeData;}
    public function getAssunto(){return $this->assunto;}
    public function getDescricao(){return $this->descricao;}
    public function getStatus(){return $this->status;}

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

    /**
     *
    //  */
    // public function enviarParaAdvogado():void
    // {
    //     // TODO: implement here
    // }

    // /**
    //  *
    //  */
    // public function recusarTicket():void
    // {
    //     // TODO: implement here
    // }

    // /**
    //  *
    //  */
    // public function revisarTicket():void
    // {
    //     // TODO: implement here
    // }
}
