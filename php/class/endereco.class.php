<?php

require_once "global.php";
/**
 *
 */
class Endereco extends Comum implements JsonSerializable
{

    public function Endereco($estado, $cidade, $bairro, $rua, $idestado, $idcidade, $idbairro, $idrua)
    {
        $this->setEstado($estado);
        $this->setCidade($cidade);
        $this->setBairro($bairro);
        $this->setRua($rua);
        $this->setIdEstado($idestado);
        $this->setIdCidade($idcidade);
        $this->setIdBairro($idbairro);
        $this->setIdRua($idrua);
    }

    /**
     * @var void
     */
    private $rua;

    /**
     * @var lista
     */
    private $bairro;

    /**
     * @var void
     */
    private $cidade;

    /**
     * @var void
     */
    private $estado;
    /**
     * @var void
     */
    private $idrua;

    /**
     * @var lista
     */
    private $idbairro;

    /**
     * @var void
     */
    private $idcidade;

    /**
     * @var void
     */
    private $idestado;

    /**
     * @var void
     */
    // private $condominio;


    public function getRua(){return $this->rua;}

    public function getBairro(){return $this->bairro;}

    public function getCidade(){return $this->cidade;}

    public function getEstado(){return $this->estado;}

    public function setRua($cadastro):bool
    {
        if (!empty($cadastro))
        {    
            if (validarString($cadastro))
            {
                $this->rua = $cadastro;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function setBairro($cadastro):bool
    {
        //var_dump($cadastro);
        if (!empty($cadastro))
        {    
            if (validarString($cadastro))
            {
                $this->bairro = $cadastro;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function setCidade($cadastro):bool
    {
        if (!empty($cadastro))
        {    
            if (validarString($cadastro))
            {
                $this->cidade = $cadastro;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function setEstado($cadastro):bool
    {
        if (!empty($cadastro))
        {    
            if (validarString($cadastro))
            {
                $this->estado = $cadastro;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

    public function setIdEstado($id): bool
    {
        if (!empty($id))
        {
            if (preg_match('/^[0-9]*$/',$id))
            {
                $this->idestado = $id;
                return TRUE;
            }
        }
        return FALSE;
    }
    public function setIdCidade($id): bool
    {
        if (!empty($id))
        {
            if (preg_match('/^[0-9]*$/',$id))
            {
                $this->idcidade = $id;
                return TRUE;
            }
        }
        return FALSE;
    }
    public function setIdBairro($id): bool
    {
        if (!empty($id))
        {
            if (preg_match('/^[0-9]*$/',$id))
            {
                $this->idbairro = $id;
                return TRUE;
            }
        }
        return FALSE;
    }
    public function setIdRua($id): bool
    {
        if (!empty($id))
        {
            if (preg_match('/^[0-9]*$/',$id))
            {
                $this->idrua = $id;
                return TRUE;
            }
        }
        return FALSE;
    }
    public function notNullGenerator()
    {
        yield $this->rua;
        yield $this->bairro;
        yield $this->cidade;
        yield $this->estado;
    }
        public function notNullShowable()
    {
        yield $this->rua;
        yield $this->bairro;
        yield $this->cidade;
        yield $this->estado;
        yield $this->idrua;
        yield $this->idbairro;
        yield $this->idcidade;
        yield $this->idestado;
    }
}
