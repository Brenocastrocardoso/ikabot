<?php

require_once "global.php";
/**
 *
 */
abstract class Parceiro extends Pessoa
{

    /**Classe abstrata implementada para a implementação dos relatorios no
     *
     */
    abstract public function gerarRelatorio();

}
