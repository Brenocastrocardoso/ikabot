<?php

require_once "global.php";
require_once "apartamento.class.php";
require_once "endereco.class.php";
/**
 *
 */
class Predio extends Comum implements JsonSerializable
{

    public function Predio($id, $endereco, $cadastroCelesc, $cadastroAgua)
    {
        $this->setId($id);
        $this->setEndereco($endereco);
        $this->setCadastroCelesc($cadastroCelesc);
        $this->setCadastroAgua($cadastroAgua);
    }

    /**
     * @var void
     */
    private $endereco;
    // private $endereco;

    /**
     * @var lista
     */
    private $apartamentos;

    /**
     * @var void
     */
    private $cadastroCelesc;

    /**
     * @var void
     */
    private $cadastroAgua;

    /**
     * @var void
     */
    private $id;



    public function getApartamentos(){return $this->apartamentos;}

    public function getEndereco(){return $this->endereco;}

    public function getCadastroCelesc(){return $this->cadastroCelesc;}

    public function getCadastroAgua(){return $this->cadastroAgua;}

    public function getId(){return $this->id;}

    // public function getCondominio(){return $this->condominio;}

    // public function setCondominio():void
    // {
    //     // TODO: implement here
    // }

    public function addApartamento($apartamento):bool
    {
        if (($apartamento instanceof Apartamento) && $apartamento->isValid())
        {
            if ($this->apartamentos == NULL)
                $this->apartamentos = new ArrayObject();
            $this->apartamentos->append($apartamento);
            return TRUE;
        }
        return FALSE;
    }


    public function setCadastroAgua($cadastro):bool
    {
        if (!empty($cadastro))
        {    
            if (preg_match('/^[0-9-]{12}$/',$cadastro))
            {
                $this->cadastroAgua = $cadastro;
                return TRUE;
            }
        }
        return FALSE;
    }
    // public function setNumero($cadastro):bool
    // {
    //     if (!empty($cadastro))
    //     {    
    //         if (preg_match('/^[0-9]{5}$/',$cadastro))
    //         {
    //             $this->numero = $cadastro;
    //             return TRUE;
    //         }
    //     }
    //     return FALSE;
    // }

    public function setEndereco($nendereco):bool
    {
        if (($nendereco instanceof Endereco) && $nendereco->isValid())
        {
            $this->endereco = $nendereco;
            return TRUE;
        }
        return FALSE;
    }
    public function setId($id): bool
    {
        if (!empty($id))
        {
            if (preg_match('/^[0-9]*$/',$id))
            {
                $this->id = $id;
                return TRUE;
            }
        }
        return FALSE;
    }

    //PRECISA REVISAR A VALIDACAO
    public function setCadastroCelesc($cadastro):bool
    {
        if (!empty($cadastro))
        {    
            if (preg_match('/^[0-9-]{12}$/',$cadastro))
            {
                $this->cadastroAgua = $cadastro;
                return TRUE;
            }
        }
        return FALSE;
    }
    
    

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

    public function notNullGenerator()
    {
        yield $this->endereco;
    }

    public function notNullShowable()
    {
        yield $this->endereco;
        yield $this->id;
    }
}
