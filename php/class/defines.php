<?php

function clearInput($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


abstract class Comum
{
    public function isValid():bool
    {
        foreach ($this->notNullGenerator() as $i)
        {
            if ($i == NULL)
                return FALSE;
        }
        return TRUE;
    }

    public function isStorageable():bool
    {
        foreach ($this->notNullGeneratorDb() as $i)
        {
            if ($i == NULL)
                return FALSE;
        }
        return TRUE;
    }

    public function isShowable()
    {
        foreach ($this->notNullShowable() as $i)
        {
            if ($i == NULL)
                return FALSE;
        }
        return TRUE;
    }

    abstract public function notNullGenerator();
}

function validarString($String)
{
        if (!empty($String))
        {    
            if (preg_match('/^[a-zA-Z0-9-ãõâôáàéíóú.ç() ]*$/',$String))
            {
                return TRUE;
            }
        }
        return FALSE;
}

function validarData($nData)
{
    if (!empty($nData))
    {
        if (preg_match('/^20[0-9]{2}-[0-9]{2}-[0-9]{2}/',$nData))
        {
            return TRUE;
        }
    }
    return FALSE;   
}

function verificaLogin()
{
    require_once("usuario.class.php");

    session_start();

    if(isset($_SESSION['Usuario']))
    {
        $ident = unserialize($_SESSION['Usuario']);

        if(!$ident->isValid())
            header("location: ../login.html");
        else
            return $ident;
    }else
        header("location: ../login.html");
}

	const ACADEMIA = array(
							"number"=>4,
							"position"=>9,
							"speedup"=>2,
						);
	const ARMAZEM = array(
							"number"=>7,
							"position"=>5,
							"speedup"=>2,
						);

	const QUARTEL = array(
							"number"=>6,
							"position"=>6,
							"speedup"=>2,
						);
	
	const MURALHA = array(
							"number"=>8,
							"position"=>14,
							"speedup"=>2,
						);
	
	const PORTO = array(
							"number"=>3,
							"position"=>1,
							"speedup"=>2,
						);

class Unidades
{
	public $lanceiro;

	public function Unidades($lanceiro)
	{
		$this->lanceiro = $lanceiro;
	}

	public function getPackage()
	{
		$string = 
			"301="."0"."&".
			"302="."0"."&".
			"303="."0"."&".
			"304="."0"."&".
			"305="."0"."&".
			"306="."0"."&".
			"307="."0"."&".
			"308="."0"."&".
			"309="."0"."&".
			"310="."0"."&".
			"311="."0"."&".
			"312="."0"."&".
			"313="."0"."&".
			"315=".$this->lanceiro;
		return $string;
	}

};

?>