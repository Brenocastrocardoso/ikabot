<?php

require_once "global.php";
require_once "banco.class.php";
/**
 *
 */
Class Conta extends Comum implements JsonSerializable
{

	/**
     * @var void
     */
    private $agencia;

    /**
     * @var void
     */
    private $conta;

    /**
     * @var void
     */
    private $banco;

    const DIGITOS_AGENCIA_MAX = 10;
    const DIGITOS_AGENCIA_MIN = 4;
    const DIGITOS_CONTA_MAX = 10;
    const DIGITOS_CONTA_MIN = 5;

    public function notNullGenerator()
    {
        yield $this->agencia;
        yield $this->conta;
        yield $this->banco;
    }

    public function Conta($nagencia, $nconta, $nbanco)
    {
    	$this->setAgencia($nagencia);
    	$this->setConta($nconta);
    	$this->setBanco($nbanco);
    }

    /**
     * @var void
     */
    public function setBanco($nbanco):bool
    {
        if (($nbanco instanceof Banco) && $nbanco->isValid())
        {
            $this->banco = ($nbanco);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @var void
     */
    public function setAgencia($nagencia):bool
    {
    	$nagencia = clearInput($nagencia);
        if (!empty($nagencia))
        {    
            if (preg_match('/^[0-9-]{'.Conta::DIGITOS_AGENCIA_MIN.','.Conta::DIGITOS_AGENCIA_MAX.'}$/',$nagencia))
            {
                $this->agencia = $nagencia;
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @var void
     */
    public function getAgencia()
    {
    	return $this->agencia;
    }

    /**
     * @var void
     */
    public function setConta($nconta):bool
    {
    	$nconta = clearInput($nconta);
        if (!empty($nconta))
        {    
            if (preg_match('/^[0-9-]{'.Conta::DIGITOS_CONTA_MIN.','.Conta::DIGITOS_CONTA_MAX.'}$/',$nconta))
            {
                $this->conta = $nconta;
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getConta()
    {
    	return $this->conta;
    }

    public function getBanco()
    {
    	return $this->banco;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

}
