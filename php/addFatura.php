<?php

require_once "class/fatura.class.php";
require_once "class/global.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
	if (!empty($_POST["vencimento"]) && !empty($_POST["status"]))
	{
		$ident_cur = unserialize($_SESSION["Identidade"]);

		if ($ident_cur->getStatus()=="Administrador"){

			$fat = new Fatura();
			if (!empty($_POST["vencimento"])){
				if(!$fat->setVencimento($_POST["vencimento"])){
					echo '{"msg": "Não foi possivel inserir o campo Vencimento"", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["status"])){
				if(!$fat->setStatus($_POST["status"])){
					echo '{"msg": "Não foi possivel inserir o campo Status", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["juros"])){
				if(!$fat->setJuros($_POST["juros"])){
					echo '{"msg": "Não foi possivel inserir o campo Juros", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["multa"])){
				if(!$fat->setMulta($_POST["multa"])){
					echo '{"msg": "Não foi possivel inserir o campo Multa", "status:"Erro"}';
					return;
				}
			}
			if (!empty($_POST["boleto_nr"])){
				if(!$fat->setBoleto($_POST["boleto_nr"])){
					echo '{"msg": "Não foi possivel inserir o campo Boleto", "status:"Erro"}';
					return;
				}
			}
			if(!$fat->inserirDb())
				echo '{"msg": "Não foi possivel inserir os dados no Banco de Dados", "status:"Erro"}';
			else
				echo '{"msg": "Cadastro realizado com sucesso", "status": "Sucesso"}';
		}
		else
		{
			echo '{"msg": "Permissão Negada", "status":"Erro"}';
		}
	}
	else
	{
		echo '{"msg": "Os campos Vencimento e Status são obrigatórios", "status":"Sucesso"}';
	}
}