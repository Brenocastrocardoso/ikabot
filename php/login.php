<?php

require_once "class/usuario.class.php";
require_once "class/defines.php";

$login = FALSE;
if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
    if (!empty($_POST["login"]) && !empty($_POST["senha"]))
    {

        $ident = new Usuario();
        $ident->setLogin($_POST["login"]);
        $ident->setSenha($_POST["senha"]);
        
        if ($ident->isValid())
        {
            $login = $ident->login();
        }
    }
}

if ($login)
    echo    '{"msg": "Login feito com sucesso. Carregando", "status": "Sucesso", "tipo": "'.$ident->privilegio.'"}';
else
    echo  '{"msg": "Login ou senha não encontrados.", "status": "Erro"}';