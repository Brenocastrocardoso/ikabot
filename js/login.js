$(document).ready(function()
{
	$("#buttonLogin").click(function()
	{
		login($('#inputLogin').val(), $('#inputPassword').val());
	});

	$("#inputPassword").keyup(function(event)
	{ 
		if(event.keyCode==13 || event.wich==13){ 
			login($('#inputLogin').val(), $('#inputPassword').val());
		}
	});

	function login(llogin, lsenha)
	{
		var request = $.ajax(
		{
			url: "php/login.php",
			method: "POST",
			data: {login: llogin, senha: lsenha},
			dataType: "json"
		});

		request.done(function( resp ) 
		{
			if(resp.status == "Sucesso")
			{
				$("#resultado").html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Sucesso!</h4>'+resp.msg+'</div>');
				if(resp.tipo == "admin")
					setTimeout(function(){ location.href = "admin/index.php"; }, 1500);
				else if(resp.tipo == "member")
					setTimeout(function(){ location.href = "member/index.php"; }, 1500);
			}
			else if(resp.status == "Erro")
				$("#resultado").html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-ban"></i> Erro!</h4>'+resp.msg+'</div>');
		});
	}
});