<?php
require_once("../php/class/defines.php");
if(($ident = verificaLogin())->isValid())
{
      
    $dados = "";
    // echo "<pre>";
    var_dump($dados);
    ?>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>AdminLTE 2 | Dashboard</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="../vendor/bootstrap/dist/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" type="text/css" href="../vendor/font-awesome/css/font-awesome.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="../vendor/adminLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
     <link rel="stylesheet" href="../vendor/adminLTE/dist/css/skins/_all-skins.min.css">
     <!-- Morris chart -->
     <link rel="stylesheet" href="../vendor/adminLTE/plugins/morris/morris.css">
     <!-- Date Picker -->
     <link rel="stylesheet" href="../vendor/adminLTE/plugins/datepicker/datepicker3.css">
     <!-- Daterange picker -->
     <link rel="stylesheet" href="../vendor/adminLTE/plugins/daterangepicker/daterangepicker.css">
     <!-- bootstrap wysihtml5 - text editor -->
     <link rel="stylesheet" href="../vendor/adminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

     <!-- DataTables -->
     <link rel="stylesheet" href="../vendor/adminLTE/plugins/datatables/dataTables.bootstrap.css">

     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" type="text/css" href="../css/geral.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>IKABOT</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Painel <b>IKABOT</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle hidden-md-up" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu" data-toggle="tooltip" title="Notificações" data-placement="bottom">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-danger">10</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                    page and may cause design problems
                </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-users text-red"></i> 5 new members joined
            </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-shopping-cart text-green"></i> 25 sales made
        </a>
    </li>
    <li>
      <a href="#">
        <i class="fa fa-user text-red"></i> You changed your username
    </a>
</li>
</ul>
</li>
<li class="footer"><a href="#">View all</a></li>
</ul>
</li>

<!-- User Account: style can be found in dropdown.less -->
            <!-- <li class="dropdown user user-menu" data-toggle="tooltip" title="Saldo" data-placement="bottom">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-money"></i>
                <span>R$ 100.000,00</span>
              </a>
          </li> -->
          <li><a href="#"><span>Perfil</span></a></li>
          <li><a href="../php/logout.php"><span>Sair</span></a></li>

      </ul>
  </div>
</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MENU</li>
      <li class="active"><a href='index.php'><i class='fa fa-money'></i> <span>Contas</span></a></li>
      <li><a href='abrirChamado.php'><i class='fa fa-user'></i> <span>Abrir chamados</span></a></li>
  </ul>
</section>
<!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
   <!-- Row Faturas -->
   <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Contas</h3>
        </div>
        <!-- Conteúdo Principal -->
        <div class="box-body table-responsive">
            <table id="example10" class="table table-bordered">
              <thead>
                <tr  class="bg-primary">
                  <th>ID</th>
                  <th>Valor</th>
                  <th>Vencimento</th>
                  <!--th>Estado</th>
                  <th>Ações</th-->
              </tr>
          </thead>
          <tbody>
            <tr class="bg-success">
              <td>1</td>
              <td>20,94</td>
              <td>20/05/1994</td>
              <!--td>Paga</td>
              <td><span class="btn btn-danger btn-xs">Arquivar</span></td-->
          </tr>
          <tr class="bg-info">
              <td>2</td>
              <td>20,94</td>
              <td>20/05/1994</td>
              <!--td>Aberto</td>
              <td><span class="btn btn-success btn-xs">Pagar</span></td-->
          </tr>
          <tr  class="bg-danger">
              <td>3</td>
              <td>20,94</td>
              <td>20/05/1994</td>
              <!--td>Vencida</td>
              <td><span class="btn btn-success btn-xs">Pagar</span></td-->
          </tr>
      </tbody>
      <tfoot>
        <tr  class="bg-primary">
          <th>ID</th>
          <th>Valor</th>
          <th>Vencimento</th>
          <!--th>Estado</th>
          <th>Ações</th-->
      </tr>
  </tfoot>
</table>
</div><!-- / Conteúdo principal -->
</div>
</div>
</div> <!-- / Row Faturas -->
</div>
<!-- /.row -->
<!-- END CUSTOM TABS -->
</section>


<!-- Início modal depósito -->
<div class="modal fade" id="modalFormularioDeposito" tabindex="-1" role="dialog" aria-labelledby="modalFormularioDeposito">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-success" id="myModalLabel">Solicitar operação de depósito</h4>
    </div>
    <form id="formDepositImage" action="php/criaDeposito.php" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="depositValue">Total a depositar</label>
              <input type="text" class="form-control" id="textValueDeposit" placeholder="Valor total do depósito">
          </div>
          <div class="form-group" id="divDepositFile">
              <label for="fileDepositImage">Comprovante de depósito</label>
              <div id="divFileDepositImage">
                <input type="file" name="fileDepositImage" id="fileDepositImage" >
                <p class="help-block">Insira seu comprovante de depósito, o quanto antes inserir, antes seu depósito será validado.</p>
            </div>
        </div>
        <div class="form-group" id="divDepositProgress">
          <label for="progressBarFile">Enviando arquivo... Aguarde</label>
          <div class="progress progress-sm active">
              <div id="progressBar" class="progress-bar progress-bar-aqua progress-bar-striped progressBar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
              </div>
          </div>
      </div>
  </div>
  <!-- /.box-body -->
</div>          
<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
  <button type="submit" class="btn btn-success" id="buttonDeposit">Depositar dinheiro</button>
</div>
</form>
</div>
</div>
</div>

<!-- Fim modal depósito -->

<!-- Início modal depósito -->
<div class="modal fade" id="modalFormularioResgate" tabindex="-1" role="dialog" aria-labelledby="modalFormularioResgate">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-warning" id="myModalLabel">Solicitar operação de retirada</h4>
      </div>
      <div class="modal-body">
          <form role="form">
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputPassword1">Total a retirar</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Valor total para retirada para conta corrente">
            </div>
            <div class="form-group">
                <label for="conta">Conta</label>
                <select id="selectConta">
                    <option value="1">Banco do Brasil - 1635</option>
                    <option value="2">Banco Inter - 0000-0</option>
                    <option value="3">Bradesco - 0000-0</option>
                    <option value="4" selected>Itaú - 31216-7</option>
                </select> 
            </div>
        </div>
        <!-- /.box-body -->
    </form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
  <button type="button" class="btn btn-warning">Retirar dinheiro</button>
</div>
</div>
</div>
</div>

<!-- Fim modal depósito -->

<!-- /.content-wrapper -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Versão</b> 0.8
</div>
<strong>Copyright &copy; 2017 <a href="index.html">B3L</a>.</strong> Todos os direitos reservados.
</footer>

<!-- ./wrapper -->

<!-- jQuery 3.1.1 -->
<script
src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>
<!-- Mascará -->
<script type="text/javascript" src="../vendor/jquery/dist/jquery.mask.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../vendor/adminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- DataTables -->
<script src="../vendor/adminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../vendor/adminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../vendor/adminLTE/dist/js/adminlte.min.js"></script>

<script type="text/javascript">
    <?php
     /*   array_filter($array, function(&$array) {
            unset($array['id']);
        });*/
    ?>
    jsonFaturas = {"status":"success","data": <?php echo json_encode((array) $dados); ?>}

    var my_data = [
    { 
      "id": "1",
      "nick":"batata assada",
      "email":"batata33@gmail"
    },
    { 
      "id": "2",
      "nick":"batata frita",
      "email":"batata34@gmail"
    }
      ];

// $('#example').DataTable( {
// data: my_data.data
// } );
</script>

<script type="text/javascript">
  $(function () {
    $('#example10').DataTable({
      "processing": true,
      "data"      : my_data,//jsonFaturas.data.debitos,
      "columns": [
        { "data": "id" },
        { "data": "nick" },
        { "data": "email" }
        ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
          "sNext": "Próximo",
          "sPrevious": "Anterior",
          "sFirst": "Primeiro",
          "sLast": "Último"
      },
      "oAria": {
          "sSortAscending": ": Ordenar colunas de forma ascendente",
          "sSortDescending": ": Ordenar colunas de forma descendente"
      }
  }
});
});
</script>

<script src="../js/script.js"></script>

</body>

</html>
<?php
}
else
    header("location: ../php/logout.php");
?>